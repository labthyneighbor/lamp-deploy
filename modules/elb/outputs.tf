output "elb-name" {
  value       = aws_elb.lamp-elb.name
  description = "The ID of the runner security group"
}
