variable "vpc-id" {
  type = string
  description = "The ID of the VPC"
  default = "vpc-36c0f850"
}

variable "sg_name" {
  description = "Name of the security group"
}
